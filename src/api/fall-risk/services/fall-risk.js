'use strict';

/**
 * fall-risk service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::fall-risk.fall-risk');
