'use strict';

/**
 * fall-risk router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::fall-risk.fall-risk');
