'use strict';

/**
 *  fall-risk controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::fall-risk.fall-risk');
