module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '4331afeb959f040c1b99fbb25b01ea49'),
  },
});
